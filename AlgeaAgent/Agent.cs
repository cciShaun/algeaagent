﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AlgeaAgent
{
    class Agent
    {

        public static Agent instance;
        private ELog l;
    public Agent (){
    //  InitializeComponent();
      l = new ELog();
      l.initLog();
      if (instance == null)
      {
          instance = this;
      }

    }

    public Thread SocketThread;
    SocketListener SListener;

    Thread ProcessThread;
    public ProcessMonitor PMonitor;
    public void startServer(){
      //Init variables
      SListener = new SocketListener();

      SocketThread = new System.Threading.Thread(SListener.Listen);
      SocketThread.IsBackground = true;
      SocketThread.Start();

      PMonitor = new ProcessMonitor();
      ProcessThread = new Thread(PMonitor.Monitor);
      ProcessThread.Start();
    }

    public void stopServer(){
      SListener.Wrapup();

      //Stop SocketThread
      if (SocketThread != null){
        SocketThread.Abort();
      }


    }
    void OnDisable(){
      stopServer();
    }
    
    }
}
