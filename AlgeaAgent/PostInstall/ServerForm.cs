﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PostInstall
{
    public partial class ServerForm : Form
    {
        public ServerForm()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox1.Text == null || textBox1.Text == ""){
                return;
            }

            string command = "/c \\\\" + textBox1.Text + "\\wpkg\\wpkg.js /install:statusreport&pause";
            string adminLink = "\\\\" + textBox1.Text +"\\wpkg\\adminCMD.lnk";
            System.Diagnostics.Process.Start(adminLink, command);
            this.Close();
        }
    }
}
