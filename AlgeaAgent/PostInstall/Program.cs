﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.AccessControl;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using IWshRuntimeLibrary;

namespace PostInstall
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                //Start service
                System.Diagnostics.Process.Start("CMD.exe", "/c net start algeaservice");
            }catch (Exception e){

            }

            try
            {
                /*
                WshShell shell = new WshShell();
                string StartupPath = @"\Microsoft\Windows\Start Menu\Programs\StartUp\algeaagentShortcut.lnk";
                string shortcutPath = (string)shell.SpecialFolders.Item((object)"allusersprofile") + StartupPath;
                IWshShortcut shortcut = (IWshShortcut)shell.CreateShortcut(StartupPath);
                shortcut.TargetPath = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles) + @"\Computer Concepts Inc\AlgeaAgent\AlgeaAgent\AlgeaAgent.exe";
                shortcut.Save();
                 * */
                object shDesktop = (object)"Desktop";
                WshShell shell = new WshShell();
                string shortcutAddress = Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData) + @"\Microsoft\Windows\Start Menu\Programs\Startup\AlgeaAgent.lnk";
                IWshShortcut shortcut = (IWshShortcut)shell.CreateShortcut(shortcutAddress);
                shortcut.Description = "New shortcut for a Notepad";
                shortcut.TargetPath = @"%programfiles%\Computer Concepts Inc\AlgeaAgent\AlgeaAgent\AlgeaAgent.exe";
                shortcut.Save();
            }
            catch (Exception e)
            {

            }

            try
            {
                //Folder Permission Changes
                string path = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData), "Algea");
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }
                var directoryInfo = new DirectoryInfo(path);
                var DirectorySecurity = directoryInfo.GetAccessControl();
                var fileSystemRule = new FileSystemAccessRule("Users", FileSystemRights.Modify, InheritanceFlags.ObjectInherit, PropagationFlags.InheritOnly, AccessControlType.Allow);

                DirectorySecurity.AddAccessRule(fileSystemRule);
                directoryInfo.SetAccessControl(DirectorySecurity);
            }
            
            catch (Exception e)
            {

            }
            try
            {
                //Start agent
                string dir = Path.Combine("C:\\Program Files", "computer concepts inc", "AlgeaAgent", "AlgeaAgent");
                string command = "/c cd \"" + dir + "\"&start algeaagent.exe";
                System.Diagnostics.Process.Start("CMD.exe", command);
            }
            catch (Exception e)
            {

            }
            /*
            ServerForm sf = new ServerForm();
            sf.Activate();
            sf.ShowDialog();
            */
        }
    }
}
