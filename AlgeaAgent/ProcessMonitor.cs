﻿using EncryptKey;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AlgeaAgent
{
    class ProcessMonitor
    {
        List<MonitorBot> MBList = new List<MonitorBot>();
        List<MonitorBot> RemovalList = new List<MonitorBot>();
        int TimeOutLength = 1800; //30 minutes
        public void Monitor()
        {
            while (true)
            {
                if (MBList.Count > 0)
                {
                    foreach (MonitorBot bot in MBList){
                        bool flag = false;

                        try {
                            flag = bot.p.HasExited;
                        }catch (Exception e){
                            try
                            {
                                //Callback Error
                                IPEndPoint serverEndPoint = new IPEndPoint(IPAddress.Parse(bot.ip), 1758);
                                Socket CallBack = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                                CallBack.Connect(serverEndPoint);
                                System.Threading.Thread.Sleep(1000);
                                string msg = bot.ID + "|3|21|Process crashed - " + e.ToString();
                                msg = StringEncrypter.Encrypt(msg);
                                byte[] CallbackMsg = Encoding.UTF8.GetBytes(msg);
                                CallBack.Send(CallbackMsg);
                                System.Threading.Thread.Sleep(1000);
                                CallBack.Disconnect(true);
                                //Remove the monitor from list
                                Destroy(bot);
                            }
                            catch (Exception ex)
                            {

                            }
                            continue;
                        }
                        
                        if (flag)
                        {
                            Destroy(bot);
                            try
                            {
                                //Callback Success
                                IPEndPoint serverEndPoint = new IPEndPoint(IPAddress.Parse(bot.ip), 1758);
                                Socket CallBack = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                                CallBack.Connect(serverEndPoint);
                                System.Threading.Thread.Sleep(1000);
                                string msg = bot.ID + "|3|0|Process completed.";
                                msg = StringEncrypter.Encrypt(msg);
                                byte[] CallbackMsg = Encoding.UTF8.GetBytes(msg);
                                CallBack.Send(CallbackMsg);
                                System.Threading.Thread.Sleep(1000);
                                CallBack.Disconnect(true);
                            }
                            catch (Exception e)
                            {

                            }
                        }

                        bot.time++;

                        if (bot.time >= TimeOutLength)
                        {
                            //Remove from list
                            Destroy(bot);

                            try
                            {
                                //Callback Error
                                IPEndPoint serverEndPoint = new IPEndPoint(IPAddress.Parse(bot.ip), 1758);
                                Socket CallBack = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                                CallBack.Connect(serverEndPoint);
                                System.Threading.Thread.Sleep(1000);
                                string msg = bot.ID + "|3|20|Process timed out.";
                                msg = StringEncrypter.Encrypt(msg);
                                byte[] CallbackMsg = Encoding.UTF8.GetBytes(msg);
                                CallBack.Send(CallbackMsg);
                                System.Threading.Thread.Sleep(1000);
                                CallBack.Disconnect(true);
                            }
                            catch (Exception e)
                            {
                                try
                                {
                                    using (StreamWriter sw = File.AppendText(@"c:\programdata\algea\algealog.txt"))
                                    {
                                        sw.WriteLine(e);
                                    }
                                }
                                catch
                                {

                                }
                            }
                        }
                    }
                    //Remove timed out items
                    foreach (MonitorBot bot in RemovalList)
                    {
                        MBList.Remove(bot);
                        //Kill process?
                    }
                    RemovalList.Clear();

                }
            Thread.Sleep(1000);
            }
        }

        void Destroy(MonitorBot m)
        {
            RemovalList.Add(m);
        }

        

        public void MonitorProcess(Process mProcess, string serverIP, string TaskID){
            MonitorBot mb = new MonitorBot();
            mb.ip = serverIP;
            mb.p = mProcess;
            mb.ID = TaskID;
            MBList.Add(mb);
        }
    }

    class MonitorBot
    {
       public Process p;
       public string ip;
       public string ID;
       public int time = 0;
    }
}
