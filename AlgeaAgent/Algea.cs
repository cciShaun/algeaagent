﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;
using System.Drawing;

namespace AlgeaAgent
{
    class Algea : ApplicationContext
    {
        NotifyIcon nIcon;
        string ListeningIP;
        public Algea()
        {
            MenuItem configMenuItem = new MenuItem("Configuration", new EventHandler(ShowConfig));
            MenuItem exitMenuItem = new MenuItem("Exit", new EventHandler(Exit));

            nIcon = new NotifyIcon();
            nIcon.Icon = Properties.Resources.AgentIcon;
            nIcon.ContextMenu = new ContextMenu(new MenuItem[] { configMenuItem, exitMenuItem });
            nIcon.Visible = true;

            Agent AlgeaA = new Agent();
            AlgeaA.startServer();
        }

        private void Exit(object sender, EventArgs e)
        {
            nIcon.Visible = false;
            Application.Exit();
        }
        public static void Exit(){
            Application.Exit();
        }
        Form1 configWindow = new Form1();
      
        void ShowConfig(object sender, EventArgs e)
        {
            
            // If we are already showing the window, merely focus it.
            if (configWindow.Visible)
            {
                configWindow.Activate();
            }
            else
            {
                configWindow.ShowDialog();
            }
        }

        public void SetIP(string s)
        {
            ListeningIP = s;
            configWindow.SetIP(s);
        }

    }
}
