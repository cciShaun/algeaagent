using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using System.Net;
using System.Net.Sockets;

namespace AlgeaService
{
  public partial class AlgeaService : ServiceBase
  {
    [DllImport("advapi32.dll", SetLastError = true)]
    private static extern bool SetServiceStatus(IntPtr handle, ref ServiceStatus serviceStatus);
    public static AlgeaService instance;

    private ELog l;
    public AlgeaService (){
      InitializeComponent();
      l = new ELog();
      l.initLog();
      instance = this;
    }

    protected override void OnStart(string[] args){
      ServiceStatus serviceStatus = new ServiceStatus();
      serviceStatus.dwCurrentState = ServiceState.SERVICE_STOPPED;
      serviceStatus.dwWaitHint = 100000;
      SetServiceStatus(this.ServiceHandle, ref serviceStatus);

      l.LogMessage("Starting AlgeaAgent Service.");

      serviceStatus.dwCurrentState = ServiceState.SERVICE_START_PENDING;
      serviceStatus.dwWaitHint = 100000;
      SetServiceStatus(this.ServiceHandle, ref serviceStatus);

      startServer();

      serviceStatus.dwCurrentState = ServiceState.SERVICE_RUNNING;
      SetServiceStatus(this.ServiceHandle, ref serviceStatus);

      l.LogMessage("Service started.");
    }

    Thread SocketThread;
    SocketListener SListener;
    Thread TaskHandlerThread;
    AlgeaTaskManager AlgeaTMgr;
    public void startServer(){
      //Init variables
      SListener = new SocketListener();
      AlgeaTMgr = AlgeaTaskManager.Instance;

      SocketThread = new System.Threading.Thread(SListener.Listen);
      SocketThread.IsBackground = true;
      SocketThread.Start();

      TaskHandlerThread = new System.Threading.Thread(AlgeaTMgr.ManageTasks);
      //TaskHandlerThread.IsBackground = true;
      TaskHandlerThread.Start();
    }



    public void stopServer(){
      SListener.Wrapup();
      AlgeaTMgr.EndThread();
      //Stop SocketThread
      if (SocketThread != null){
        SocketThread.Abort();
      }
      if (TaskHandlerThread != null){
        TaskHandlerThread.Abort();
      }

    }
    void OnDisable(){
      stopServer();
    }

    private void InitializeComponent()
    {
            // 
            // AlgeaService
            // 
            this.ServiceName = "AlgeaService";

    }

  }
  public enum ServiceState
    {
        SERVICE_STOPPED = 0x00000001,
        SERVICE_START_PENDING = 0x00000002,
        SERVICE_STOP_PENDING = 0x00000003,
        SERVICE_RUNNING = 0x00000004,
        SERVICE_CONTINUE_PENDING = 0x00000005,
        SERVICE_PAUSE_PENDING = 0x00000006,
        SERVICE_PAUSED = 0x00000007,
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct ServiceStatus
    {
        public long dwServiceType;
        public ServiceState dwCurrentState;
        public long dwControlsAccepted;
        public long dwWin32ExitCode;
        public long dwServiceSpecificExitCode;
        public long dwCheckPoint;
        public long dwWaitHint;
    }
}
