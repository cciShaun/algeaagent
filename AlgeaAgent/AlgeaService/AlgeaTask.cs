using System;
using System.Management;
using System.Collections.ObjectModel;

namespace AlgeaService
{
  public class AlgeaTask
  {
    eType type;
    string tname;
    Days[] days;
    TimeSpan time;
    bool OneTime;
    string command;

    public AlgeaTask(eType newType, string n, Days[] d, TimeSpan t){
      type = newType;
      tname = n;
      days = d;
      time = t;
    }
    public AlgeaTask(string n, string c){
      tname = n;
      command = c;
      type = eType.cmd;
      OneTime = true;
      DateTime tmp = DateTime.Now;
      time = new TimeSpan(tmp.Hour, tmp.Minute, tmp.Second);
    }
    public string GetName(){
      return tname;
    }
    public eType GetTType(){
      return type;
    }
    public string GetCommand(){
      return command;
    }
    public bool Compare(AlgeaTask c){
      //TODO: Compare what day will come next this or c.
      return true;
    }
  }
  public enum Days
  {
    Sunday,
    Monday,
    Tuesday,
    Wednesday,
    Thursday,
    Friday,
    Saturday
  }
  public enum eType
  {
    wpkg,
    cmd,
    other
  }
}
