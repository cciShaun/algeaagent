using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;
using System.Text;
using EncryptKey;


namespace AlgeaService
{
  public class SocketListener
  {
    volatile bool keepReading = false;
    Socket listener;
    Socket handler;
    private string authString = "R3achYggdr@sil!";
    private ELog l;

    public void Listen(){
      l = new ELog();
      l.initLog();
      string data;
      byte[] IData = new Byte[1024];

      IPAddress[] ipArray = Dns.GetHostAddresses(getIPAddress());
      IPEndPoint localEndPoint = new IPEndPoint(ipArray[0], 1756);
      //Create a TCP/IP Socket
      listener = new Socket(ipArray[0].AddressFamily, SocketType.Stream, ProtocolType.Tcp);
      l.LogMessage("Listening on: " + ipArray[0]);
      /*
             * Bind the socket to the local endpoint and
             * listen for incoming connections.
             * */
      try{
        listener.Bind(localEndPoint);
        listener.Listen(10);// Max length of pending connections

        while(true){
          keepReading = true;
          l.LogMessage("Waiting for Connection.");
          //Program is suspended while waiting for an incoming connection.
          handler = listener.Accept();
          data = null;

          //An incoming connection needs to be processed.
          while (keepReading){
            l.LogMessage("Receiving.");
            IData = new Byte[1024];
            int bytesRec = handler.Receive(IData);

            if (bytesRec <= 0){
              keepReading = false;
              handler.Disconnect(true);
              break;
            }

            data += Encoding.ASCII.GetString(IData, 0, bytesRec);
            data = StringEncrypter.Decrypt(data);
            //l.LogMessage(data);
            if (data.ToString().Split('|')[0] == authString){
              data = StringEncrypter.Encrypt(data);
              AlgeaTaskManager.Instance.AddTask(data);
            }
            if (data.IndexOf("<EOF>") > -1){
              break;
            }
            Thread.Sleep(1);
          }
          Thread.Sleep(1);
        }
      }catch (Exception e){
        l.LogMessage(e.ToString(), 420);
      }

    }

    public void Wrapup(){// Used to end the service
      keepReading = false;

      if (handler != null && handler.Connected){
        handler.Disconnect(false);
      }
    }

    private string getIPAddress()
        {
            IPHostEntry host;
            string localIP = "";
            host = Dns.GetHostEntry(Dns.GetHostName());

            foreach (IPAddress ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    localIP = ip.ToString();
                }
            }
            return localIP;
        }
  }
}
