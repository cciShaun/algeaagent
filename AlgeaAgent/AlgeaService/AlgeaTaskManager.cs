using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;
using System.Text;
using EncryptKey;

namespace AlgeaService
{
  public class AlgeaTaskManager{
    private static AlgeaTaskManager instance = new AlgeaTaskManager();
    static AlgeaTaskManager (){

    }
    public AlgeaTaskManager(){

    }
    public static AlgeaTaskManager Instance{
      get{
        return instance;
      }
    }
    Queue<String> TaskList;
    ELog l;
    bool enabled = false;
    public void ManageTasks(){
      l = new ELog();
      l.initLog();
      enabled = true;
      TaskList = new Queue<String>();
      while (enabled){
          if (TaskList.Count > 0)
          {

              string info = TaskList.Dequeue();
              //l.DebugMessage(info);
              string data = StringEncrypter.Decrypt(info);
              string[] s = data.Split('|');

              try
              {
                  IPAddress[] ipArray = Dns.GetHostAddresses(getIPAddress());
                  IPEndPoint localEndPoint = new IPEndPoint(ipArray[0], 1757);
                  Socket server = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                  server.Connect(localEndPoint);
                  System.Threading.Thread.Sleep(1000);
                  byte[] message = Encoding.UTF8.GetBytes(info);
                  server.Send(message);
                  server.Disconnect(true);
              }
              catch (Exception e)
              {
                  try
                  {
                      //Callback ERROR - Agent isn't running?
                      IPEndPoint serverEndPoint = new IPEndPoint(IPAddress.Parse(s[7]), 1758);
                      Socket CallBack = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                      CallBack.Connect(serverEndPoint);
                      System.Threading.Thread.Sleep(1000);
                      string msg = s[8] + "|1|-1|AlgeaS received message and was unable to send to Agent - " + e.ToString();
                      msg = StringEncrypter.Encrypt(msg);
                      byte[] CallbackMsg = Encoding.UTF8.GetBytes(msg);
                      CallBack.Send(CallbackMsg);
                      System.Threading.Thread.Sleep(1000);
                      CallBack.Disconnect(true);
                  }
                  catch
                  {
                      l.LogMessage(e.ToString());
                  }
                  continue;
              }

              try
              {
                  //Callback to server SUCCESS
                  IPEndPoint serverEndPoint2 = new IPEndPoint(IPAddress.Parse(s[7]), 1758);
                  Socket CallBack2 = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                  CallBack2.Connect(serverEndPoint2);
                  System.Threading.Thread.Sleep(1000);
                  string msg2 = s[8] + "|1|0|AlgeaS received message and successfully sent to Agent.";
                  msg2 = StringEncrypter.Encrypt(msg2);
                  byte[] CallbackMsg2 = Encoding.UTF8.GetBytes(msg2);
                  CallBack2.Send(CallbackMsg2);
                  System.Threading.Thread.Sleep(1000);
                  CallBack2.Disconnect(true);
              }catch (Exception e){
                  l.LogMessage(e.ToString());
              }
          }
          System.Threading.Thread.Sleep(1000);
      }
    }

    public void AddTask(string data){
      TaskList.Enqueue(data);
    }
    public void EndThread(){
      enabled = false;
    }

    private string getIPAddress()
        {
            IPHostEntry host;
            string localIP = "";
            host = Dns.GetHostEntry(Dns.GetHostName());

            foreach (IPAddress ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    localIP = ip.ToString();
                }
            }
            return localIP;
        }
  }
}
