using System;
using System.Diagnostics;
using System.IO;


namespace AlgeaService{
  public class ELog{
    public EventLog elog;

    public void initLog (){
      //Configure Event Log
      elog = new EventLog();
        if (!Directory.Exists("C:\\Program Files (x86)\\Computer Concepts\\AlgeaAgent\\Log")){
            try
            {
                Directory.CreateDirectory("C:\\Program Files (x86)\\Computer Concepts\\AlgeaAgent\\Log");
            }
            catch(Exception e)
            {
                throw new IOException("Algea encountered an error while creating log directory.", e);
            }
        }
      if (!EventLog.SourceExists("C:\\Program Files (x86)\\Computer Concepts\\AlgeaAgent\\Log\\" + DateTime.Today + ".txt"))
      {
         try
         {
              EventLog.CreateEventSource("C:\\Program Files (x86)\\Computer Concepts\\AlgeaAgent\\Log\\" + DateTime.Today + ".txt", DateTime.Today + "Log");
         }
         catch (Exception e)
         {
             throw new IOException("Algea encountered an error while creating log file.", e);
         }
      }
      elog.Source = "C:\\Program Files (x86)\\Computer Concepts\\AlgeaAgent\\Log\\" + DateTime.Today + ".txt";
      elog.Log = DateTime.Today + "Log";

      //Configue Debug Log.
        string directory = Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData);
      if (!File.Exists(Path.Combine(directory, "Algea", "AlgeaLog.txt"))){
          try
          {
              File.Create(Path.Combine(directory, "Algea", "AlgeaLog.txt"));
          }
          catch (Exception e)
          {
              throw new IOException("Algea encountered an error while creating log file.", e);
          }
      }
    }

    public void DebugMessage(string text)
    {
        string directory = Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData);
        string mydocpath = Path.Combine(directory, "Algea", "AlgeaLog.txt");
        string msg = DateTime.Now.ToLongTimeString();
        msg += " : " + text;
        try
        {
            using (StreamWriter sw = File.AppendText(mydocpath))
            {
                sw.WriteLine(msg);
            }
        }
        catch
        {

        }
  }

    public void LogMessage(String message)
    {
        String log = "\r\nLog Entry: ";
        log += DateTime.Now.ToLongTimeString();
        log += "\n" + message + "\n";
        log += "-----------------------------------------------";
        try
        {
            elog.WriteEntry(log);
        }
        catch
        {

        }
    }

    public void LogMessage(String message, int id)
    {
        String log = "\r\nLog Entry: ";
        log += DateTime.Now.ToLongTimeString();
        log += "\n" + message + "\n";
        log += "-----------------------------------------------";
        try
        {
            elog.WriteEntry(log, EventLogEntryType.Information, id);
        }
        catch
        {

        }
    }
  }
}
