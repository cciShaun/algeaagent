﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EncryptKey
{
    class GenerateKey
    {
        public static string GenKey()
        {
            string input = "" + DateTime.Now.Hour + DateTime.Now.Day + DateTime.Now.Year + DateTime.Now.Month;
            Console.WriteLine(input);
            String key = GenerateKey.GetKey(input);
            return key;
        }
        static void printKey(List<string> l)
        {
            String key = "";
            foreach (string c in l)
            {
                key += c;
            }
            Console.WriteLine(key);
        }

        public static String GetKey(string v)
        {
            Tables t = new Tables();
            char[] tmp = v.ToCharArray();
            List<string> keyString = new List<string>();
            //Add char array to list.
            foreach (char c in tmp){
                keyString.Add(c.ToString());
            }
            //Add inverse date to the end of the tmp
            for (int i = v.Length - 1; i > -1; i--)
            {
                keyString.Add(tmp[i].ToString());
            }
            //printKey(keyString);
            // Shift digits around
            for (int i = 0; i < keyString.Count-1; i++)
            {
                if (i % 3 == 1)
                {
                    string tempStr = keyString[i];
                    keyString[i] = keyString[i - 1];
                    keyString[i - 1] = tempStr;
                }
                if (i % 3 == 2)
                {
                    string tempStr = keyString[i];
                    keyString[i] = keyString[i + 1];
                    keyString[i + 1] = tempStr;
                }
            }
            //printKey(keyString);
            // Convert ASCII to dec
            for (int i = 0; i < keyString.Count; i++)
            {
                keyString[i] = t.GetDEC(keyString[i]);
            }
            //printKey(keyString);
            //Make real conversion to oct
            for (int i = 0; i < keyString.Count; i++)
            {
                string temp = "";
                temp = Convert.ToString(Convert.ToInt32(keyString[i]), 8);
                keyString[i] = temp;
            }
            //printKey(keyString);
            //Convert to value - index to base 13 from base 8
            for (int i = 0; i < keyString.Count; i++)
            {
                int base8Val = Convert.ToInt32(keyString[i]);
                int base10Val = Convert.ToInt32(base8Val.ToString(), 8);
                base10Val -= i;
                keyString[i] = ConvertBase(base10Val, 13);
            }
            //printKey(keyString);
            //convert to ASCII treating the base 13 as base 16 and adding the length - index to the starting value
            for (int i = 0; i < keyString.Count; i++)
            {
                string val = ConvertBase( Convert.ToInt32(keyString[i], 16) + (keyString.Count - i), 16);
                keyString[i] = val;
            }
           // printKey(keyString);
            for (int i = 0; i < keyString.Count; i++)
            {

                keyString[i] = t.GetHEX(keyString[i]);
            }
            String key = "";
            
            foreach (string c in keyString)
            {
                key += c;
            }
            return key;
        }

        static string ConvertBase(int num, int baseNum)
        {
            const string symbols = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
            if (baseNum < 2 || symbols.Length < baseNum)
            {
                throw new ArgumentOutOfRangeException(baseNum.ToString());
            }

            if (num < 0)
            {
                throw new ArgumentOutOfRangeException(num.ToString());
            }

            var resultLength = 1 + Math.Max((int)Math.Log(num, baseNum), 0);
            var result = new char[resultLength];
            int index = resultLength - 1;
            do
            {
                result[index--] = symbols[(int)(num % baseNum)];
                num /= baseNum;
            } while (num != 0);

            return new string(result);
        }
    }
}
