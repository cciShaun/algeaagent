﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace AlgeaFirewallConfiguration
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length > 0)
            {
                if (args[0].ToString() == "/u")
                {
                    FirewallController.RemoveFWRule("AlgeaAgent");
                    FirewallController.RemoveFWRule("AlgeaService");
                }
            }
            else
            {
                int port = 1757;
                FirewallController.NewFWRule(port, "AlgeaAgent");

                port = 1756;
                FirewallController.NewFWRule(port, "AlgeaService");
            }
        }
    }
}
