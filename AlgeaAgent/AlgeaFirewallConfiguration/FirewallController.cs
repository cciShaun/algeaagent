﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NetFwTypeLib;
using System.Collections;

namespace AlgeaFirewallConfiguration
{
    class FirewallController
    {
        private static INetFwProfile sm_fwProfile = null;
        public static bool IsPortOpen(int port)
        {
            EnsureSetup();

            Type progID = Type.GetTypeFromProgID("HNetCfg.FwMgr");
            INetFwMgr firewall = Activator.CreateInstance(progID) as INetFwMgr;
            INetFwOpenPorts ports = firewall.LocalPolicy.CurrentProfile.GloballyOpenPorts;
            IEnumerator portEnumerate = ports.GetEnumerator();

            while ((portEnumerate.MoveNext()))
            {
                INetFwOpenPort currentPort = portEnumerate.Current as INetFwOpenPort;
                if (currentPort.Port == port)
                {
                    return true;
                }
            }
            return false;
        }

        public static void OpenPort(int port, string applicationName)
        {
            EnsureSetup();

            if (IsPortOpen(port))
            {
                return;
            }

            INetFwOpenPort openPort = GetInstance("INetOpenPort") as INetFwOpenPort;
            openPort.Port = port;
            openPort.Protocol = NET_FW_IP_PROTOCOL_.NET_FW_IP_PROTOCOL_TCP;
            openPort.Name = applicationName;

            INetFwOpenPorts openPorts = sm_fwProfile.GloballyOpenPorts;
            openPorts.Add(openPort);
            
        }

        public static void ClosePort(int port)
        {
            EnsureSetup();

            if (!IsPortOpen(port))
            {
                return;
            }

            INetFwOpenPorts ports = sm_fwProfile.GloballyOpenPorts;
            ports.Remove(port, NET_FW_IP_PROTOCOL_.NET_FW_IP_PROTOCOL_TCP);
        }

        public static void NewFWRule(int port, string Appname)
        {
            INetFwRule fwRule = (INetFwRule)Activator.CreateInstance(Type.GetTypeFromProgID("HNetCfg.FWRule"));
            fwRule.Action = NET_FW_ACTION_.NET_FW_ACTION_ALLOW;
            fwRule.Description = "Allows Algea to communicate to central controller.";
            fwRule.Direction = NET_FW_RULE_DIRECTION_.NET_FW_RULE_DIR_IN;
            fwRule.Enabled = true;
            fwRule.InterfaceTypes = "All";
            fwRule.Name = Appname;
            fwRule.Protocol = (int)NET_FW_IP_PROTOCOL_.NET_FW_IP_PROTOCOL_TCP;
            fwRule.LocalPorts = port.ToString();
            INetFwPolicy2 fwPolicy = (INetFwPolicy2)Activator.CreateInstance(Type.GetTypeFromProgID("HNetCfg.FwPolicy2"));
            fwPolicy.Rules.Add(fwRule);

        }

        public static void RemoveFWRule(string FWName)
        {
            INetFwPolicy2 fwPolicy = (INetFwPolicy2)Activator.CreateInstance(Type.GetTypeFromProgID("HNetCfg.FwPolicy2"));
            fwPolicy.Rules.Remove(FWName);
        }

        private static object GetInstance(string typeName)
        {
            Type tpResult = null;

            switch (typeName)
            {
                case "INetFwMgr":
                    tpResult = Type.GetTypeFromCLSID(new Guid("{304CE942-6E39-40D8-943A-B913C40C9CD4}"));
                    return Activator.CreateInstance(tpResult);
                case "INetAuthApp":
                    tpResult = Type.GetTypeFromCLSID(new Guid("{EC9846B3-2762-4A6B-A214-6ACB603462D2}"));
                    return Activator.CreateInstance(tpResult);
                case "INetOpenPort":
                    tpResult = Type.GetTypeFromCLSID(new Guid("{0CA545C6-37AD-4A6C-BF92-9F7610067EF5}"));
                    return Activator.CreateInstance(tpResult);
                default:
                    throw new Exception("Unknown type name: " + typeName);
            }
        }

        private static void EnsureSetup()
        {
            if (sm_fwProfile != null)
            {
                return;
            }

            INetFwMgr fwMgr = GetInstance("INetFwMgr") as INetFwMgr;
            sm_fwProfile = fwMgr.LocalPolicy.CurrentProfile;
        }
    }
}
