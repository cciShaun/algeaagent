﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlgeaAgent
{
    class ELog
    {
        public EventLog elog;
        string mydocpath = Path.Combine( Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData),"Algea", "AlgeaLog.txt");

        public void initLog()
        {
            //Check the file path exists
            if (!Directory.Exists(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "Algea")))
            {
                try
                {
                    Directory.CreateDirectory(Path.Combine( Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData),"Algea"));
                }
                catch (Exception e)
                {
                    System.Windows.Forms.MessageBox.Show("Algea encountered an error while creating directory: " + e.Message);
                    Algea.Exit();
                }
            }
            //Configue Debug Log.
            if (!File.Exists(mydocpath))
            {
                try
                {
                    File.Create(mydocpath);
                }
                catch (Exception e)
                {
                    System.Windows.Forms.MessageBox.Show("Algea encountered an error while creating log file: " + e.Message);
                    Algea.Exit();
                }
            }
        }

        public void DebugMessage(string text)
        {
            string msg = DateTime.Now.ToLongTimeString();
            msg += " : " + text;
            initLog();
            try { 
            using (StreamWriter sw = File.AppendText(mydocpath))
            {
                sw.WriteLine(msg);
            }
            }
            catch (Exception e)
            {

            }
        }

        public void LogMessage(String message)
        {
            String log = "\r\nLog Entry: ";
            log += DateTime.Now.ToLongTimeString();
            log += "\n" + message + "\n";
            log += "-----------------------------------------------";
            initLog();
            try
            {

            using (StreamWriter sw = File.AppendText(mydocpath))
            {
                sw.WriteLine(log);
            }
            }
            catch (Exception e)
            {

            }
        }

        public void LogMessage(String message, int id)
        {
            String log = "\r\nLog Entry: ";
            log += DateTime.Now.ToLongTimeString();
            log += "\n" + message + "\n";
            log += "-----------------------------------------------";
            initLog();
            try
            {

            using (StreamWriter sw = File.AppendText(mydocpath))
            {
                sw.WriteLine(log);
            }
            }
            catch (Exception e)
            {

            }
        }
    }
}
