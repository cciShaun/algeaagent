﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace EncryptKey
{
    class StringEncrypter
    {

        private static byte[] _salt = GenSalt();

        private static byte[] GenSalt()
        {
            return Encoding.ASCII.GetBytes("90BE81818A958E81ACB3A198819F8F8F517E8D8C86958592837625237E7681757C69616855645B575C415415645C5433334A1A161B1A1A46");
        }
        public static string Encrypt(string plainString){
            if (string.IsNullOrEmpty(plainString))
            {
                throw new ArgumentNullException("Empty String Provided.");
            }

            string outStr = null;

            RijndaelManaged aesAlg = null;

            try
            {
                Rfc2898DeriveBytes key = new Rfc2898DeriveBytes(GenerateKey.GenKey(), _salt);

                aesAlg = new RijndaelManaged();
                aesAlg.Key = key.GetBytes(aesAlg.KeySize / 8);

                ICryptoTransform encryptor = aesAlg.CreateEncryptor(aesAlg.Key, aesAlg.IV);

                using (MemoryStream msEncrypt = new MemoryStream())
                {
                    // prepend the IV
                    msEncrypt.Write(BitConverter.GetBytes(aesAlg.IV.Length), 0, sizeof(int));
                    msEncrypt.Write(aesAlg.IV, 0, aesAlg.IV.Length);
                    using(CryptoStream csEncrypt = new CryptoStream(msEncrypt, encryptor,CryptoStreamMode.Write)){
                        using (StreamWriter swEncrypt = new StreamWriter(csEncrypt)){
                            swEncrypt.Write(plainString);
                        }
                    }
                outStr = Convert.ToBase64String(msEncrypt.ToArray());
                }
            }
            finally
            {
                if (aesAlg != null)
                {
                    aesAlg.Clear();
                }
            }
            return outStr;
        }
        public static string Decrypt(string encryptedString)
        {
            if (string.IsNullOrEmpty(encryptedString))
            {
                throw new ArgumentNullException("Empty String Provided.");
            }

            RijndaelManaged aesAlg = null;

            string plainString = null;

            try
            {
                Rfc2898DeriveBytes key = new Rfc2898DeriveBytes(GenerateKey.GenKey(), _salt);

                byte[] bytes = Convert.FromBase64String(encryptedString);
                using (MemoryStream msDecrypt = new MemoryStream(bytes)){
                    aesAlg = new RijndaelManaged();
                    aesAlg.Key = key.GetBytes(aesAlg.KeySize / 8);

                    aesAlg.IV = ReadByteArray(msDecrypt);
                    ICryptoTransform decryptor = aesAlg.CreateDecryptor(aesAlg.Key, aesAlg.IV);
                    using (CryptoStream csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read)){
                        using (StreamReader srDecrypt = new StreamReader(csDecrypt)){
                            plainString = srDecrypt.ReadToEnd();
                        }
                    }
                }
            }
            finally
            {
                if (aesAlg != null)
                {
                    aesAlg.Clear();
                }
            }
            return plainString;
        }

        private static byte[] ReadByteArray(Stream s)
        {
            byte[] rawLength = new byte[sizeof(int)];
            if (s.Read(rawLength, 0, rawLength.Length) != rawLength.Length)
            {
                throw new SystemException("Stream did not contain properly formatted byte array");
            }

            byte[] buffer = new byte[BitConverter.ToInt32(rawLength, 0)];
            if (s.Read(buffer, 0, buffer.Length) != buffer.Length)
            {
                throw new SystemException("Did not read byte array properly");
            }

            return buffer;
        }
    }
}
