﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using EncryptKey;
using System.IO;
using System.Threading.Tasks;

namespace AlgeaAgent
{
    class SocketListener
    {
        volatile bool keepReading = false;
        Socket listener;
        Socket handler;
        private string authString = "R3achYggdr@sil!";
        private ELog l;

        public void Listen()
        {
            l = new ELog();
            l.initLog();
            string data;
            byte[] IData = new Byte[1024];

            IPAddress[] ipArray = Dns.GetHostAddresses(getIPAddress());
            IPEndPoint localEndPoint = new IPEndPoint(IPAddress.Parse(getIPAddress()), 1757);
            //Create a TCP/IP Socket
            listener = new Socket(IPAddress.Parse(getIPAddress()).AddressFamily, SocketType.Stream, ProtocolType.Tcp);
            l.LogMessage("Listening on: " + getIPAddress());
            Program.a.SetIP(localEndPoint.ToString());
            /*
                   * Bind the socket to the local endpoint and
                   * listen for incoming connections.
                   * */
            try
            {
                listener.Bind(localEndPoint);
                listener.Listen(10);// Max length of pending connections

                while (true)
                {
                    keepReading = true;
                    l.LogMessage("Waiting for Connection.");
                    //Program is suspended while waiting for an incoming connection.
                    handler = listener.Accept();
                    data = null;

                    //An incoming connection needs to be processed.
                    while (keepReading)
                    {
                        l.LogMessage("Receiving.");
                        IData = new Byte[1024];
                        int bytesRec = handler.Receive(IData);

                        if (bytesRec <= 0)
                        {
                            keepReading = false;
                            handler.Disconnect(true);
                            break;
                        }

                        data += Encoding.ASCII.GetString(IData, 0, bytesRec);
                        //Decrypt the data that was received.
                        data = StringEncrypter.Decrypt(data);
                        String[] s = data.Split('|');
                        //l.LogMessage(s[1]);
                        if (s[0] == authString)
                        {
                            ExecuteJob(s);
                            s = null;
                        }
                        if (data.IndexOf("<EOF>") > -1)
                        {
                            break;
                        }
                        Thread.Sleep(1);
                    }
                    Thread.Sleep(1);
                }
            }
            catch (Exception e)
            {
                l.LogMessage(e.ToString(), 420);
            }

        }

        public void ExecuteJob(String[] s)
        {
            string AuthDomain = "";
            string DisconnectDomain = "";
            if (s[2] == "Domain")
            {

                AuthDomain = @"net use \\" + s[7] + @" /user:" + s[5] + " " + s[6] + " & ";
                DisconnectDomain = @" & net use /delete \\" + s[7];
            }

            Process newProcess = new Process();
            newProcess.StartInfo.CreateNoWindow = true;
            newProcess.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
            newProcess.StartInfo.UseShellExecute = false;
            newProcess.StartInfo.FileName = "cmd";
            //newProcess.StartInfo.RedirectStandardOutput = true;
            //newProcess.StartInfo.RedirectStandardError = true;

            if (s[2] == "Elevate" || s[2] == "Domain")
            {
                newProcess.StartInfo.WorkingDirectory = @"C:\";
                newProcess.StartInfo.UserName = s[3];
                string pass = s[4];
                System.Security.SecureString pwd = new System.Security.SecureString();
                for (int i = 0; i < pass.Length; i++)
                {
                    pwd.AppendChar(pass[i]);
                }
                newProcess.StartInfo.Password = pwd;

            }

            newProcess.StartInfo.Arguments = "/c " + AuthDomain + s[1] + DisconnectDomain;

            try
            {
                newProcess.Start();
                Agent.instance.PMonitor.MonitorProcess(newProcess, s[7], s[8]);
            }
            catch (Exception e)
            {
                try
                {
                    //Callback ERROR - 20
                    IPEndPoint serverEndPoint = new IPEndPoint(IPAddress.Parse(s[7]), 1758);
                    Socket CallBack = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                    CallBack.Connect(serverEndPoint);
                    System.Threading.Thread.Sleep(1000);
                    string msg = s[8] + "|2|20|" + e.ToString();
                    msg = StringEncrypter.Encrypt(msg);
                    byte[] CallbackMsg = Encoding.UTF8.GetBytes(msg);
                    CallBack.Send(CallbackMsg);
                    System.Threading.Thread.Sleep(1000);
                    CallBack.Disconnect(true);
                }catch(Exception ex){
                    try
                    {
                        using (StreamWriter sw = File.AppendText(@"c:\programdata\algea\algealog.txt"))
                        {
                            sw.WriteLine(ex + " -- " + e);
                        }
                    }
                    catch
                    {

                    }
                }
            }

            //s = null;
        }

        public void Wrapup()
        {// Used to end the service
            keepReading = false;

            if (handler != null && handler.Connected)
            {
                handler.Disconnect(false);
            }
        }

        private string getIPAddress()
        {
            IPHostEntry host;
            string localIP = "";
            host = Dns.GetHostEntry(Dns.GetHostName());

            foreach (IPAddress ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    localIP = ip.ToString();
                }
            }
            return localIP;
        }
    }
}
